const express = require('express')
const app = express()
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static('public'));

app.get('/',function( req,res){
  res.sendFile(__dirname + '/index.html');
});
app.get('/display',function( req,res){
  res.sendFile(__dirname + '/display.html');
});

app.get('/admin',function( req,res){
  res.sendFile(__dirname + '/admin.html');
});

io.on('connection',function(socket){
  console.log('a user connected');
  socket.on('disconnect',function(){
    console.log('user disconnected')
  });

  socket.on('chat message',function(msg){
    console.log('message: ' + msg);
    io.emit('chat message', msg);

    io.emit('slide add', { hello: 'world' });
  });


  socket.on('slide add',function(data){
    io.emit('slide add',data);
  });

});


http.listen(80,function(){
  console.log('listening on port 80');
});
